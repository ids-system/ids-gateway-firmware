# ids-gateway-firmware

- Receive  Lora packets from ids-device (alarm, low batt, etc)
- Automatic Multi Wifi AP connect (list of SSID/passwords)
- Transmit ids-device state to ids-server via http request over wifi
- Transmit 'alive' requests to server for both gateway and device
- Firmware upgradable via wifi (http self hosted upload page)

## hardware

esp32 , oled display, lora module
(heltec_lora32 board)

- https://robotzero.one/heltec-wifi-lora-32/

## librairies

- https://github.com/espressif/arduino-esp32
- https://github.com/sandeepmistry/arduino-LoRa
- https://github.com/ThingPulse/esp8266-oled-ssd1306

## pictures

