/**
 * IDS Gateway Firmware
 *
 *  Created on: 2018.08.16
 *
 */

#include <WiFi.h>
#include <WiFiMulti.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>
#include <SPI.h>
#include <LoRa.h>

#include "myhttpclient.h"
#include "myoleddisplay.h"
#include "webserver.h"
#include "credentials.h"
#include "structures.h"

const char* host = "j7-ids";

#define SCK     5
#define MISO    19
#define MOSI    27
#define SS      18
#define RST     14
#define DI00    26
#define BAND    433E6
#define PABOOST true

MyHttpClient 	httpClient;
MyOledDisplay 	display(0x3c, 4, 15);
WebServer 		server(80);
WiFiMulti 		wifiMulti;

bool 			wifiConnected=false;
uint8_t     	wifiCounter = 0;
uint16_t 		packetCounter=0;

ulong 			lastPacketReceived=0;
ulong       	lastGatewayAliveReport = 0;
ulong 			lastScreenUpdate=0;

DeviceDataStruct state = {
	.header="J7",
	.timer=0,
	.deviceBattVoltage=0.0f,
	.deviceBattPercent=0,
	.devicePirState=false,
	.deviceAlarmActive=false,
	.deviceLowBatt=false
};

void waitWifi(uint16_t timeout) {
	// wait for WiFi connection
	while(wifiMulti.run() != WL_CONNECTED && timeout>0) {
			timeout--;
			delay(1);
	}
	if(timeout==0)
		Serial.print("waitWifi");
}

void handleRadioData(){

	// read packet(s)
	if (!LoRa.available())
		return;

	DeviceDataStruct newState;

	int readBytes = LoRa.readBytes((uint8_t*) &newState, sizeof(DeviceDataStruct));
	if(readBytes!=sizeof(DeviceDataStruct)){
		Serial.print("Invalid packet size: ");
		Serial.println(String(readBytes));
		return;
	}
	if(strcmp(newState.header,"J7")!=0){
		Serial.print("Invalid packet header: ");
		Serial.println(newState.header);
		return;
	}

	state=newState;
	packetCounter++;
	lastPacketReceived=millis();

	Serial.print("Received packet ");
	Serial.print(packetCounter);
	Serial.print(" with RSSI ");
	Serial.println(LoRa.packetRssi());

	stateRequest();
}

void gatewayIsAliveRequest() {
	waitWifi(1000);
 	httpClient.httpGet(URL_GATEWAY_ALIVE_REQUEST);
}

void stateRequest() {

	//TODO: retry if wifi unavailable or http 500?
	waitWifi(1000);

	StateRequestStruct request = {
		.rssi=(uint8_t) LoRa.packetRssi(),
		.timerGateway=(uint64_t) esp_timer_get_time(),
		.timerDevice=(uint64_t) state.timer,
		.deviceBattVoltage=(float)state.deviceBattVoltage,
		.deviceBattPercent=(uint8_t)state.deviceBattPercent,
		.devicePirState=(uint8_t)state.devicePirState,
		.deviceAlarmActive=(uint8_t)state.deviceAlarmActive,
		.deviceLowBatt=(uint8_t)state.deviceLowBatt
	};

	int httpCode=httpClient.httpPost(URL_DEVICE_STATE_REQUEST, (uint8_t*)&request,sizeof(StateRequestStruct));
}

void setup() {

	pinMode(16,OUTPUT);
	pinMode(25,OUTPUT);
	digitalWrite(16, LOW);

	Serial.begin(115200);
	Serial.setDebugOutput(true);

	SPI.begin(SCK,MISO,MOSI,SS);
	LoRa.setPins(SS,RST,DI00);

	if (!LoRa.begin(BAND))
	{
		Serial.println("Starting LoRa failed!");
		while (1) {
			delay(1000);
		}
	}

	Serial.println("LoRa Initial success!");

	/*use mdns for host name resolution*/
	if (!MDNS.begin(host)) {
		Serial.println("Error setting up MDNS responder!");
		while (1) {
			delay(1000);
		}
	}

	Serial.println("mDNS responder started");

	delay(50);
	digitalWrite(16, HIGH);

	display.init();
	display.flipScreenVertically();
	display.setFont(ArialMT_Plain_10);
	display.drawLogoFrame();
	display.display();

	/*return index page which is stored in serverIndex */
	server.on("/", HTTP_GET, []() {
		server.sendHeader("Connection", "close");
		server.send(200, "text/html", serverIndex);
	});

	/*handling uploading firmware file */
	server.on("/update", HTTP_POST, []() {
		server.sendHeader("Connection", "close");
		server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
		ESP.restart();
	}, []() {
		HTTPUpload& upload = server.upload();
		if (upload.status == UPLOAD_FILE_START) {
			Serial.printf("Update: %s\n", upload.filename.c_str());
			if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
				Update.printError(Serial);
			}
		} else if (upload.status == UPLOAD_FILE_WRITE) {
			/* flashing firmware to ESP*/
			if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
				Update.printError(Serial);
			}
		} else if (upload.status == UPLOAD_FILE_END) {
			if (Update.end(true)) { //true to set the size to the current progress
				Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
			} else {
				Update.printError(Serial);
			}
		}
	});

	httpClient.setReuse(true);
	httpClient.setTimeout(750);

	while(millis()<1500){
		delay(1);
	}

	display.drawWifiConnectFrame();
	display.display();

	Serial.println("Connecting Wifi...");

	WiFi.mode(WIFI_STA);
	wifiMulti.addAP(SSID1, SECRET1);
	wifiMulti.addAP(SSID2, SECRET2);
	wifiMulti.addAP(SSID3, SECRET3);
	wifiMulti.addAP(SSID4, SECRET4);

	while(wifiMulti.run(10000) != WL_CONNECTED){
		Serial.print(".");
		wifiCounter+=20;
		if(wifiCounter>100){
			wifiCounter=0;
		}
		display.drawWifiConnectProgressFrame(wifiCounter);
		display.display();
		delay(20);
	}

	wifiConnected=true;
	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
	server.begin();
	display.drawWifiConnectedFrame(WiFi.localIP().toString());
	display.display();

	delay(1500);
	display.drawMainFrame(&state, 0, true, 0);
	display.display();
}

void loop() {

	server.handleClient();
	display.drawMainFrame(&state, packetCounter, WiFi.status()==WL_CONNECTED, lastPacketReceived);

	//send 'gateway is alive' every minute
	if(((millis()-lastGatewayAliveReport)>(60*1000)) || (lastGatewayAliveReport==0)) {
		gatewayIsAliveRequest();
		lastGatewayAliveReport=millis();
		return;
	}

	//refresh screen @24 fps
	if((millis()-lastScreenUpdate)>(1000/24)){
		display.display();
		lastScreenUpdate=millis();
		return;
	}

	int packetSize = LoRa.parsePacket(sizeof(DeviceDataStruct));
	if(packetSize){
		Serial.println("Got data !");
		handleRadioData();
	}
}

