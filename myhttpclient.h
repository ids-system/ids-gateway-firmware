#ifndef __MY_HTTP_CLIENT_H_
#define __MY_HTTP_CLIENT_H_

#include <HTTPClient.h>


class MyHttpClient : public HTTPClient {
    public:
      MyHttpClient();
      int httpPost(String url, uint8_t* body, size_t size);
      int httpGet(String url);
};

#endif
