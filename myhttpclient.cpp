#include "myhttpclient.h"

//constructeur
MyHttpClient::MyHttpClient() : HTTPClient() {

}



int MyHttpClient::httpPost(String url, uint8_t* body, size_t size) {

	begin(url);

	Serial.print("[HTTP] POST ");
	Serial.print(url);
	Serial.print(" (");
	Serial.print(size);
	Serial.print(") ");

	//http.addHeader("Content-Type", "application/x-binary");

	int httpCode = POST(body, size);
	if(httpCode > 0) {
		Serial.printf(" code: %d\n", httpCode);
		if(httpCode == HTTP_CODE_OK) {
			String payload = getString();
			Serial.println(payload);
		}
	} else {
		Serial.printf(" error: %s\n", errorToString(httpCode).c_str());
	}

	end();
	return httpCode;
}

int MyHttpClient::httpGet(String url) {

	begin(url);

	Serial.print("[HTTP] GET ");
	Serial.print(url);

	// start connection and send HTTP header
	int httpCode = GET();

	// httpCode will be negative on error
	if(httpCode > 0) {
		Serial.printf(" code: %d\n", httpCode);
		if(httpCode == HTTP_CODE_OK) {
			String payload = getString();
			Serial.println(payload);
		}
	} else {
		Serial.printf(" error: %s\n", errorToString(httpCode).c_str());
	}

	end();
	return httpCode;
}